# Open AIS Simple Front End

A simple Open AIS front end.

![Simple front end user interface screenshot.](/docs/ui.png)


## Getting Started

Install dependencies:

```sh
npm install
```

Run the development server:

```sh
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) in your browser.

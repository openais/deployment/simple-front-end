#!/bin/bash

set -eux

API_HOST=${API_HOST:-"localhost"}
API_PORT=${API_PORT:-"9000"}

cat >/usr/share/nginx/html/config.json <<-EOF
{
    "host": "${API_HOST}",
    "port": ${API_PORT}
}
EOF

/docker-entrypoint.sh nginx -g "daemon off;"

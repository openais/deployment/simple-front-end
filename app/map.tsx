'use client'

import * as ol from 'ol'
import * as olformat from 'ol/format'
import * as ollayer from 'ol/layer'
import * as olloadingstrategy from 'ol/loadingstrategy'
import * as olsource from 'ol/source'
import * as olproj from 'ol/proj'
import { Dispatch, useContext, useEffect, useReducer, useRef, useState } from 'react'

import 'ol/ol.css'

import { ConfigContext } from './config'


interface State {
    map: ol.Map,
    status: null | 'loading' | 'loaded',
}


function initState() {
    if (typeof document !== 'undefined') {
        olproj.useGeographic()
        let map = new ol.Map({
            layers: [
                new ollayer.Tile({
                    source: new olsource.OSM(),
                }),
            ],
            view: new ol.View({
                center: [2.9, 51.6],
                zoom: 8,
            }),
        })
        return { map, status: null, api: null, elem: null }
    }
    return {} as State
    // throw Error('Tried to initialize state without a document defined')
}


type Action = {
    type: 'load',
    url: string,
    elem: HTMLElement,
    dispatch: Dispatch<Action>,
} | {
    type: 'failed',
} | {
    type: 'loaded',
}


function reducer(prevState: State, action: Action): State {
    switch (action.type) {
        case 'load':
            if (prevState.status === 'loading') {
                // still loading, do nothing
                return prevState
            } else {
                // start map initialization
                addVectorLayer(prevState.map, action.url, action.elem, action.dispatch)
                return { ...prevState, status: 'loading' }
            }
        case 'failed':
            return { ...prevState, status: null }
        case 'loaded':
            return { ...prevState, status: 'loaded' }
    }
}


function addVectorLayer(map: ol.Map, url: string, elem : HTMLElement, dispatch: Dispatch<Action>) {
    const geojsonFormat = new olformat.GeoJSON()
    const source = new olsource.Vector({
        format: geojsonFormat,
        loader: function (extent: any, resolution: any, projection: any, success: any, failure: any) {
            fetch(`${url}?limit=10000&bbox=${extent.join(',')}`, {
                headers: {
                    'Accept': 'application/geo+json',
                }
            })
                .then(res => {
                    res.json()
                        .then(json => {
                            const features = geojsonFormat.readFeatures(json)
                            source.addFeatures(features)
                            dispatch({ type: 'loaded' })
                            success(features)
                        })
                        .catch(e => {
                            console.error('Error while getting the API JSON result', e)
                            source.removeLoadedExtent(extent)
                            dispatch({ type: 'failed' })
                            failure()
                        })
                })
                .catch(e => {
                    console.log('Error while querying the API', e)
                    source.removeLoadedExtent(extent)
                    dispatch({ type: 'failed' })
                    failure()
                })
        },
        strategy: olloadingstrategy.bbox,
    })
    const layer = new ollayer.Vector({ source })
    map.addLayer(layer)
    map.setTarget(elem)
}


export default function Map({ className }: { className: string }) {
    const [state, dispatch] = useReducer(reducer, null, initState)
    const ref = useRef(null)

    const config = useContext(ConfigContext)

    useEffect(() => {
        if (config !== null && ref.current !== null) {
            dispatch({
                type: 'load',
                url: `http://${config.host}:${config.port}/collections/postgisftw.ship/items.json`,
                elem: ref.current,
                dispatch,
            })
        }
        // return () => {
        //     if (state.map !== null) {
        //         state.map.dispose()
        //     }
        // }
    }, [ref.current, config])

    return <div className={className}><div ref={ref}></div></div>
}

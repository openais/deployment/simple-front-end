export default function Page () {
    return (
        <main>
            <h2>About</h2>
            <p>A simple front end to the Open AIS database and API.</p>
        </main>
    )
}

'use client'

import { createContext, useEffect, useState } from 'react'

const initialShip = { mmsi: undefined } as { mmsi : string | undefined }

export const ShipsContext = createContext<typeof initialShip>(initialShip)


interface ShipsParams {
    children: React.ReactNode,
}

interface ShipInfo {
    mmsi : string,
    call : string | null,
    name : string | null,
}

export default function Ships ({
    children
}: Readonly<ShipsParams>) {
    const [ship, setShip] = useState<string|undefined>()
    const [options, setOptions] = useState<JSX.Element[]>([])

    const host = 'openais-dev.vliz.be'
    const port = 9000

    useEffect(() => {
        fetch(`http://${host}:${port}/collections/postgisftw.latest_positions/items.json?limit=10000`, {
                headers: {
                    'Accept': 'application/json',
                }
            })
            .then( res => res.json() )
            .then( json => {
                const o : ShipInfo[] = json.features.map( (f : any) => {
                        return {
                            mmsi: f.properties.mmsi,
                            call: f.properties.call,
                            name: f.properties.name,
                        }
                    })
                o.sort( (a, b) => a.mmsi > b.mmsi ? 1 : a.mmsi === b.mmsi ? 0 : -1 )
                setOptions(o.map( a => <option key={a.mmsi}>{a.mmsi} {a.name}</option>))
            })
    }, [])

    return (
        <>
            <select value={ship} onChange={ e => setShip(e.target.value) }>
                {options}
            </select>
            <ShipsContext.Provider value={{ mmsi: ship }}>{children}</ShipsContext.Provider>
        </>
    )

}

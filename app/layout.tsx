import type { Metadata } from "next"
import Link from "next/link"

import "./globals.css"
import styles from "./layout.module.css"

export const metadata : Metadata = {
    title: "Open AIS Simple Front End",
    description: "A simple front end to the Open AIS database and API.",
}

export default function RootLayout({
    children,
} : Readonly<{
    children: React.ReactNode
}>) {
    return (
        <html lang="en">
            <body>
                <nav className={styles.nav}>
                    <Link href="/"><h1>Open AIS</h1></Link>
                    <Link href="/about">About</Link>
                </nav>
                <div className={styles.main}>{children}</div>
            </body>
        </html>
    )
}

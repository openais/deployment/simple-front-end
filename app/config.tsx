'use client'

import { createContext, useEffect, useState } from 'react'

const initialConfig = { host: 'localhost', port: '9000' }

export const ConfigContext = createContext<typeof initialConfig|null>(initialConfig)

export default function Config ({
    children,
} : Readonly<{
    children: React.ReactNode
}>) {
    const [config, setConfig] = useState<typeof initialConfig|null>(null)
    useEffect(() => {
        if (config === null) {
            const scheme = window.location.protocol
            const host = window.location.host
            fetch(`${scheme}//${host}/config.json`)
                .then( response => response.json() )
                .then( config => setConfig(config) )
        }
    })
    return <ConfigContext.Provider value={config}>{children}</ConfigContext.Provider>
}

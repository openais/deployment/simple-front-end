import Chart from './chart'
import Config from './config'
import Ships from './ships'
import Map from './map'

import styles from "./page.module.css"

export default async function Page () {
    return (
        <main>
            <Config>
                <Map className={styles.map} />
                <Ships>
                    <Chart className={styles.chart} width={800} height={200} />
                </Ships>
            </Config>
        </main>
    )
}

'use client'

import * as d3 from 'd3'
import { useContext, useEffect, useRef, useState } from 'react'
import { ShipsContext } from './ships'


interface ChartParams {
    className? : string,
    width : number
    height : number,
    marginTop? : number
    marginRight? : number
    marginBottom? : number
    marginLeft? : number
}

export default function Chart ({
    className,
    width,
    height,
    marginTop=20,
    marginRight=20,
    marginBottom=30,
    marginLeft=40,
}: ChartParams) {
    const [data, setData] = useState<{ date : Date, value : number }[]>([])
    const ref = useRef(null)

    const host = 'openais-dev.vliz.be'
    const port = 9000

    const ship = useContext(ShipsContext)

    useEffect(() => {
        if (ship.mmsi) {
           fetch(`http://${host}:${port}/collections/postgisftw.positions/items.json?limit=10000&mmsi=${ship.mmsi}`, {
                   headers: {
                       'Accept': 'application/json',
                   }
               })
               .then( res => res.json() )
               .then( json => {
                   const dv : { date : Date, value : number }[] = json.features.map( (f : any) => {
                           return {
                               date: new Date(f.properties.time_bucket),
                               value: f.properties.sog,
                           }
                       })
                   dv.sort( (a, b) => a.date.getTime() - b.date.getTime() )
                   setData(dv)
               })
           }
    }, [ship.mmsi])
    useEffect(() => {
        const xdom = d3.extent(data, d => d.date) as [Date, Date]
        const x = d3.scaleUtc(xdom, [marginLeft, width - marginRight])
        const ydom = [0, d3.max(data, d => d.value)] as [number, number]
        // const ydom = [0, 1] as [number, number]
        const y = d3.scaleLinear(ydom, [height - marginBottom, marginTop])

        const svg = d3.select(ref.current)
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", [0, 0, width, height])
            .attr("style", "max-width: 100%; height: auto;")

        svg.selectChildren().remove()

        const area = d3.area<{ date : Date, value : number}>()
            .x(d => x(d.date))
            .y0(y(0))
            .y1(d => y(d.value));

        svg.append("path")
            .attr("fill", "steelblue")
            .attr("d", area(data))

        svg.append("g")
            .attr("transform", `translate(0,${height - marginBottom})`)
            .call(d3.axisBottom(x).ticks(width / 80).tickSizeOuter(0))

        svg.append("g")
            .attr("transform", `translate(${marginLeft},0)`)
            .call(d3.axisLeft(y).ticks(height / 40))
            .call(g => g.select(".domain").remove())
            .call(g => g.selectAll(".tick line").clone()
                .attr("x2", width - marginLeft - marginRight)
                .attr("stroke-opacity", 0.1))
            .call(g => g.append("text")
                .attr("x", -marginLeft)
                .attr("y", 10)
                .attr("fill", "currentColor")
                .attr("text-anchor", "start")
                .text("Speed Over Ground (SOG)"))
    }, [ref, data])

    return <svg className={className} ref={ref} />

}

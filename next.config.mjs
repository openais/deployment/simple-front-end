/** @type {import('next').NextConfig} */
const nextConfig = {
    // Generate a static website
    output: 'export',
};

export default nextConfig;

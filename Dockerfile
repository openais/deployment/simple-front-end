FROM node:20 AS base

RUN apt-get update && apt-get upgrade -y


#
# App dependencies
#
FROM base AS deps

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci


#
# App builder (static site)
#
FROM base AS builder

WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

# Disable Next.js telemetry
ENV NEXT_TELEMETRY_DISABLED 1

RUN npm run build


#
# App
#
FROM nginx AS runner

COPY scripts/start.sh ./start.sh
COPY --from=builder /app/out /usr/share/nginx/html

ENTRYPOINT [ "/start.sh" ]
